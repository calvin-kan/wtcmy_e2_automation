import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.util.concurrent.ThreadLocalRandom as Keyword
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger

'Generate random number'
String randomNumber = String.valueOf(Math.abs(new Random().nextInt() % (999999999 - 111111111)) + 111111111)

'Declare strings'
String memberEmail = (('autotesting-' + randomNumber) + GlobalVariable.BU) + '@yopmail.com'

String memberFirstNameEnglish = 'AutoTesting'

String memberLastNameEnglish = 'ENGmember'

String memberMobile = '01' + randomNumber

WebUI.openBrowser('')

'Go to WTCPH UAT'
WebUI.navigateToUrl(GlobalVariable.defaultURL)

'Go to my account'
WebUI.mouseOver(findTestObject('Home/home_MyAccountBtn'))

WebUI.enhancedClick(findTestObject('Home/home_loginBtn'))

//Registration
'click register button'
WebUI.enhancedClick(findTestObject('Login/login_RegisterButton'))

WebUI.check(findTestObject('Registration/registration_buyCardChkbox'))

'Enter mobile number'
WebUI.setText(findTestObject('OTP/otp_mobileField'), memberMobile)

'Click request OTP button'
WebUI.enhancedClick(findTestObject('Registration/registration_buyCardSubmitBtn'))

'Fill in OTP'
WebUI.setText(findTestObject('OTP/otp_otpField'), GlobalVariable.defaultOTP)

'Submit OTP'
WebUI.enhancedClick(findTestObject('OTP/otp_submitOTPBtnEShopperBuyCard'))

'Enter name with randomly generated number'
WebUI.setText(findTestObject('Registration/registration_english_firstName'), memberFirstNameEnglish)

'Enter name with randomly generated number'
WebUI.setText(findTestObject('Registration/registration_english_lastName'), memberLastNameEnglish)

'Paste email'
WebUI.sendKeys(findTestObject('Registration/registraion_EmailField'), memberEmail)

'Enter pw'
WebUI.sendKeys(findTestObject('Registration/registration_PasswordField'), GlobalVariable.defaultPassword)

WebUI.scrollToElement(findTestObject('Registration/registration_tAndCChkbox'), 0)

'Accpet T&C'
WebUI.check(findTestObject('Registration/registration_tAndCChkbox'))

WebUI.enhancedClick(findTestObject('Registration/registration_birthYearDropdownBtn'))

WebUI.enhancedClick(findTestObject('Registration/registration_birthYear1'))

'submit'
WebUI.enhancedClick(findTestObject('Registration/registration_buyCardSubmitBtn'))

WebUI.enhancedClick(findTestObject('Checkout/Payment Method/checkout_creditCardOneTimePayBtn'))

WebUI.enhancedClick(findTestObject('Checkout/checkout_PayBtn'))

WebUI.setText(findTestObject('PaymentGateway/IPGSG/ipgsg_cardHolderName'), GlobalVariable.visa_cardName)

WebUI.setText(findTestObject('PaymentGateway/IPGSG/ipgsg_cardNum1'), GlobalVariable.visa_cardNum1)

WebUI.setText(findTestObject('PaymentGateway/IPGSG/ipgsg_cardNum2'), GlobalVariable.visa_cardNum2)

WebUI.setText(findTestObject('PaymentGateway/IPGSG/ipgsg_cardNum3'), GlobalVariable.visa_cardNum3)

WebUI.setText(findTestObject('PaymentGateway/IPGSG/ipgsg_cardNum4'), GlobalVariable.visa_cardNum4)

WebUI.selectOptionByValue(findTestObject('PaymentGateway/IPGSG/ipgsg_cardExpMM'), GlobalVariable.visa_expiryMonth, false)

WebUI.selectOptionByValue(findTestObject('PaymentGateway/IPGSG/ipgsg_cardExpYY'), GlobalVariable.visa_expiryYear, false)

WebUI.setText(findTestObject('PaymentGateway/IPGSG/ipgsg_cvv'), GlobalVariable.visa_cvv)

WebUI.enhancedClick(findTestObject('PaymentGateway/IPGSG/ipgsg_submitBtn'))

'check thank you page heading visible'
WebUI.verifyTextPresent(GlobalVariable.thankYouPageHeading, false)

WebUI.delay(5)

'check if reached ac summary page'
WebUI.verifyElementVisible(findTestObject('AccountSummary/account_AccountSummaryTitle'), FailureHandling.STOP_ON_FAILURE)

KeywordLogger logger = new KeywordLogger()

logger.logInfo(memberEmail)

'change pw'
WebUI.enhancedClick(findTestObject('AccountSummary/account_changePw'))

'enter old pw'
WebUI.setText(findTestObject('ChangePassword/changePw_oldPw'), GlobalVariable.defaultPassword)

'enter new pw'
WebUI.setText(findTestObject('ChangePassword/changePw_newPw'), GlobalVariable.changePassword)

'enter new pw again'
WebUI.setText(findTestObject('ChangePassword/changePw_confirmNewPw'), GlobalVariable.changePassword)

'click update pw'
WebUI.enhancedClick(findTestObject('ChangePassword/changePw_updatePwBtn'))

'click update pw'
WebUI.enhancedClick(findTestObject('ChangePassword/changePw_updatePwBtn'))

'verify element present : pw updated message'
WebUI.verifyElementVisible(findTestObject('ChangePassword/changePw_PwUpdatedMsg'))

'mouse over the my account btn'
WebUI.mouseOver(findTestObject('Home/home_MyAccountBtn'))

'click the logout btn'
WebUI.enhancedClick(findTestObject('Home/home_logoutBtn'))

'Go to my account'
WebUI.mouseOver(findTestObject('Home/home_MyAccountBtn'))

WebUI.enhancedClick(findTestObject('Home/home_loginBtn'))

'enter login email'
WebUI.setText(findTestObject('Login/login_usernameField'), memberEmail)

'enter pw'
WebUI.setText(findTestObject('Login/login__passwordField'), GlobalVariable.changePassword)

'click login btn'
WebUI.enhancedClick(findTestObject('Login/login_loginButton'))

'mouse over the my account btn'
WebUI.mouseOver(findTestObject('Home/home_MyAccountBtn'))

'click the logout btn'
WebUI.enhancedClick(findTestObject('Home/home_logoutBtn'))

'Go to my account'
WebUI.mouseOver(findTestObject('Home/home_MyAccountBtn'))

WebUI.enhancedClick(findTestObject('Home/home_loginBtn'))

'forget pw'
WebUI.enhancedClick(findTestObject('Login/login_forgetPw'))

'click reset pw by email option'
WebUI.check(findTestObject('ForgetPassword/forgetPw_emailResetOption'))

'click next'
WebUI.enhancedClick(findTestObject('ForgetPassword/forgetPw_nextBtn'))

'set loop flag to false'
Boolean whileLoopRanOnce = false

'set email received flag to false'
Boolean resetPwEmailReceived = false

'run loop as long as email is not received'
while (resetPwEmailReceived == false) {
    'run if loop flag is true'
    if (whileLoopRanOnce == true) {
        'go to reset pw page'
        WebUI.navigateToUrl(GlobalVariable.forgetByEmailPageURL)
    }
    
    'enter email '
    WebUI.setText(findTestObject('ForgetPassword/forgetPw_emailField'), memberEmail)

    'confirm email'
    WebUI.enhancedClick(findTestObject('ForgetPassword/forgetPw_confirmEmailBtn'))

    'verify element present : reset pw email has been sent popup'
    WebUI.verifyTextPresent(GlobalVariable.pwResetToEmailMsg, false)

    'wait 10 sec'
    WebUI.delay(10)

    'go to yopmail'
    WebUI.navigateToUrl('http://www.yopmail.com/en/')

    'enter email'
    WebUI.setText(findTestObject('YopmailHomePage/yopmail_EmailField'), memberEmail)

    'click check email button'
    WebUI.enhancedClick(findTestObject('YopmailHomePage/yopmail_CheckInboxBtn'))

    WebUI.switchToFrame(findTestObject('YopmailInbox/iframe_Headers_ifmail'), 5)

    'click the link in the email'
    resetPwEmailReceived = WebUI.verifyElementPresent(findTestObject('YopmailInbox/yopmail_ResetPw'), 3, FailureHandling.OPTIONAL)

    'change loop flag to true'
    whileLoopRanOnce = true
}

'click reset pw link in the email'
WebUI.enhancedClick(findTestObject('YopmailInbox/yopmail_ResetPw'))

'switch to new tab'
WebUI.switchToWindowIndex(1)

'enter new pw'
WebUI.setText(findTestObject('CreateNewPassword/newPw_newPw'), GlobalVariable.defaultPassword)

//'enter new pw again'
//WebUI.setText(findTestObject('CreateNewPassword/newPw_Next'), GlobalVariable.defaultPassword)
'click next'
WebUI.enhancedClick(findTestObject('CreateNewPassword/newPw_Next'))

'verify element present : change pw successful msg'
WebUI.verifyTextPresent(GlobalVariable.changePwSucessfulMsg, false)

//'verify element present : the email is correct'
//WebUI.verifyTextPresent(memberEmail, false)
'click login now'
WebUI.enhancedClick(findTestObject('CreateNewPassword/newPw_LoginNowBtn'))

'fill in login'
WebUI.setText(findTestObject('Login/login_usernameField'), memberEmail)

'fill in pw'
WebUI.setText(findTestObject('Login/login__passwordField'), GlobalVariable.defaultPassword)

'click login btn'
WebUI.click(findTestObject('Login/login_loginButton'))

WebUI.mouseOver(findTestObject('Home/home_MyAccountBtn'))

WebUI.enhancedClick(findTestObject('Home/home_accountSummaryLink'))

'check if reached ac summary page'
WebUI.verifyElementText(findTestObject('AccountSummary/account_AccountSummaryTitle'), GlobalVariable.acSummary_Heading)

