<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Payment Tests</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>572d7e94-ac7f-40bf-8e5e-b88000ca3c17</testSuiteGuid>
   <testCaseLink>
      <guid>30169f17-5b43-4c06-a828-68dd80883edb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/PT - Member Login</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d62ffa29-a6b7-4fee-924f-072860b00619</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>79cd5624-bce5-4f1c-a970-4507589cf37d</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>3ae33739-7620-4d56-8aab-c893235e0ebf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/HD/PT - HD - Visa</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>371e843a-1aba-4a32-9605-aad2f8a78d13</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/HD/PT - HD - BankPayment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>df0c24ad-354d-483c-96e4-4db14817c7bc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/FD/PT - FD - Visa</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5274c718-adc1-4498-a2e7-f60d6877744a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/FD/PT - FD - BankPayment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e8f39fd3-f430-41c8-9f3e-748cfbcdb176</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/CCS/PT - CCS - Visa</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3012e869-d547-4aa0-8150-6f980c1f1d32</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/CCS/PT - CCS - BankPayment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5a291258-904e-49f9-9e73-8847ef97299d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/CCE/PT - CCE - Visa</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5b37796b-fac1-415b-a589-ef9ddb411bd1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/CCE/PT - CCE - BankPayment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>339b7ffa-011a-47b9-a2d5-94e8f5d34344</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/PT - Display Order Numbers</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
